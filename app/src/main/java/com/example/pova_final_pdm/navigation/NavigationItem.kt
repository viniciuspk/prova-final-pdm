package com.example.pova_final_pdm.navigation

import com.example.pova_final_pdm.R

sealed class NavigationItem(var route: String, var icon: Int,var title: String){
    object Cars: NavigationItem(route = PageRoute.MainPage.route, icon = R.drawable.car , title = "Cars")
    object Dashboards: NavigationItem(route = PageRoute.DashboardPage.route, icon = R.drawable.chart_pie, title = "Dashboards")
}
