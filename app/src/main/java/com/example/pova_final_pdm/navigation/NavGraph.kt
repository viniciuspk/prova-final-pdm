package com.example.pova_final_pdm.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.example.pova_final_pdm.pages.DashboardPage
import com.example.pova_final_pdm.pages.MainPage

@Composable
fun SetupNavGraph(navController: NavHostController){
    NavHost(navController = navController, startDestination = PageRoute.MainPage.route){
        composable(route =PageRoute.MainPage.route){
                MainPage()
            }
        composable(route =PageRoute.DashboardPage.route){
            DashboardPage()
        }
    }
}
