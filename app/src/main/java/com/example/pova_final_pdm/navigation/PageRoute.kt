package com.example.pova_final_pdm.navigation

sealed class PageRoute(val route: String){
    object MainPage: PageRoute(route = "main_page")
    object DashboardPage: PageRoute(route = "dashboard_page")
}
