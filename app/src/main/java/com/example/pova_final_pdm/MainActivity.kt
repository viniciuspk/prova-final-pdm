package com.example.pova_final_pdm

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.pova_final_pdm.navigation.NavigationItem
import com.example.pova_final_pdm.navigation.SetupNavGraph
import com.example.pova_final_pdm.pages.MainPage
import com.example.pova_final_pdm.ui.theme.BackgroundColor
import com.example.pova_final_pdm.ui.theme.PovafinalPDMTheme
import com.example.pova_final_pdm.ui.theme.PrimaryColor

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            Scaffold(bottomBar = { BottomNavigationAppBar(navController = navController) }) { padding ->
                Box(modifier = Modifier.padding(padding)) {
                    SetupNavGraph(navController = navController)
                }
            }
        }
    }
}

@Composable
fun BottomNavigationAppBar(navController: NavController) {
    val items = listOf(
        NavigationItem.Cars,
        NavigationItem.Dashboards
    )
    BottomNavigation(
        backgroundColor = PrimaryColor,
        contentColor = BackgroundColor
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route
        items.forEach { item ->
            BottomNavigationItem(
                icon = { Icon(painterResource(id = item.icon), contentDescription = item.title) },
                label = { Text(text = item.title) },
                selectedContentColor = BackgroundColor,
                unselectedContentColor = BackgroundColor.copy(0.4f),
                alwaysShowLabel = true,
                selected = currentRoute == item.route,
                onClick = {
                    navController.navigate(item.route) {

                        //código gringo pra evitar empilhamento infinito de pages

                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        navController.graph.startDestinationRoute?.let { route ->
                            popUpTo(route) {
                                saveState = true
                            }
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    }
                }
            )
        }
    }
}
