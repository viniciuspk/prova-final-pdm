package com.example.pova_final_pdm.data

import java.util.logging.Logger
import kotlin.math.log

object DAOVehiclesSingleton {
    private var serial : Long = 1
    private val vehicles = ArrayList<VehicleModel>()

    fun add(v : VehicleModel){
        this.vehicles.add(v)
        v.id = serial++
    }

    fun getVehicles(): ArrayList<VehicleModel> {
        return this.vehicles
    }

    fun getVehiclesById(id : Long) : VehicleModel?{
        for(v in this.vehicles){
            if(v.id == id)
                return v
        }
        return null
    }
}