package com.example.pova_final_pdm.data

import androidx.annotation.StringRes
import com.example.pova_final_pdm.R

class VehicleModel(
    val model: String,
    val price: String,
    val type: VehiclesType,
    var sold: Boolean = false,
    var id: Long = 0
) {
    constructor(id : Long) : this("", "",VehiclesType.HATCH){
        this.id = id;
    }
    private val vehiclesList = ArrayList<VehicleModel>()

    fun addVehicle(vehicle : VehicleModel){
        this.vehiclesList.add(vehicle)
    }

    fun getVehicles(): ArrayList<VehicleModel> {
        return this.vehiclesList
    }

    fun changeVehicleSoldStatus(id : Long){
        val index = vehiclesList.indexOfFirst { it.id == id }
        val vehicle = vehiclesList[index]
        vehicle.sold = !vehicle.sold
        this.vehiclesList[index] = vehicle
    }
}

enum class VehiclesType(@StringRes val type: Int) {
    HATCH(R.string.hatch),
    TRUCK(R.string.truck),
    MOTORBIKE(R.string.motor_bike),
    SEDAN(R.string.sedan),
    PICKUP_TRUCK(R.string.pickup_truck),
    VAN(R.string.van),
    SUV(R.string.suv),
}
