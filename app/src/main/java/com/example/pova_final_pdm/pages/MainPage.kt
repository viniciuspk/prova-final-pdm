package com.example.pova_final_pdm.pages

import android.graphics.Color
import androidx.annotation.StringRes
import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.KeyboardArrowDown
import androidx.compose.material.icons.outlined.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pova_final_pdm.ui.theme.*
import androidx.compose.ui.text.style.TextDecoration
import com.example.pova_final_pdm.data.DAOVehiclesSingleton
import com.example.pova_final_pdm.data.VehicleModel
import com.example.pova_final_pdm.data.VehiclesType
import com.example.pova_final_pdm.utils.CurrencyAmountInputVisualTransformation
import kotlinx.coroutines.flow.MutableStateFlow


@Preview
@Composable
fun MainPage() {
    val vehiclesTypesList = enumValues<VehiclesType>()

    var model by remember {
        mutableStateOf("")
    }

    var type by remember {
        mutableStateOf("")
    }

    var price by remember {
        mutableStateOf("")
    }

    var expanded by remember { mutableStateOf(false) }

    val _vehicles = remember { MutableStateFlow(listOf<VehicleModel>()) }
    val vehicles by remember { _vehicles }.collectAsState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp)
            .background(BackgroundColor)
    ) {
        Row(modifier = Modifier.padding(8.dp)) {
            Card(elevation = 8.dp, modifier = Modifier.fillMaxWidth()) {
                Column(
                    modifier = Modifier.padding(8.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text("Model") },
                        value = model,
                        onValueChange = { model = it })

                    Spacer(modifier = Modifier.padding(2.dp))

                    Column {
                        OutlinedTextField(
                            modifier = Modifier
                                .fillMaxWidth()
                                .clickable { expanded = !expanded },
                            value = type,
                            enabled = false,
                            onValueChange = { type = it },
                            label = { Text("Choose a type ...") },
                            readOnly = true,
                            trailingIcon = {
                                Icon(
                                    imageVector = if (expanded) {
                                        Icons.Outlined.KeyboardArrowUp
                                    } else {
                                        Icons.Outlined.KeyboardArrowDown
                                    },
                                    contentDescription = null
                                )
                            })
                        DropdownMenu(
                            expanded = expanded,
                            onDismissRequest = { expanded = false },
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            vehiclesTypesList.forEach { vehicleType ->
                                DropdownMenuItem(
                                    modifier = Modifier.fillMaxWidth(),
                                    onClick = {
                                        type = vehicleType.toString()
                                        expanded = false
                                    }
                                ) {
                                    Text(text = stringResource(id = vehicleType.type))
                                }
                                Divider()
                            }
                        }
                    }

                    Spacer(modifier = Modifier.padding(2.dp))

                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        value = price,
                        onValueChange = {
                            price = if (it.startsWith('0', ignoreCase = false)) {
                                ""
                            } else {
                                it
                            }
                        },
                        label = { Text("Price") },
                        visualTransformation = CurrencyAmountInputVisualTransformation(
                            numberOfDecimals = 2
                        ),
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword)
                    )
                    Spacer(modifier = Modifier.padding(2.dp))

                    Button(
                        modifier = Modifier.fillMaxWidth(0.5f),
                        onClick = {
                            val v = VehicleModel(model, price, VehiclesType.valueOf(type))
                            DAOVehiclesSingleton.add(v)
                            _vehicles.value = DAOVehiclesSingleton.getVehicles()
                        }) {
                        Text(text = "Submit")
                    }
                }
            }
        }

        Row(modifier = Modifier.padding(8.dp)) {
            VehiclesList(vehicles = vehicles)
        }
    }
}

@Composable
fun VehiclesList(vehicles: List<VehicleModel>) {
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(vehicles) { vehicle ->
            var itemExpanded by remember {
                mutableStateOf(false)
            }
            VehicleItemView(
                vehicle,
                onClick = { itemExpanded = !itemExpanded }

            )
            ExpandedVehicleItemView(vehicle = vehicle, isExpanded = itemExpanded)
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun VehicleItemView(vehicle: VehicleModel, onClick: () -> Unit) {
    Card(
        backgroundColor = if (vehicle.sold) {
            SoldColor
        } else {
            AvailableColor
        },
        modifier =
        Modifier
            .fillMaxWidth()
            .padding(top = 4.dp)
            .combinedClickable(onClick = onClick, onLongClick = { vehicle.sold = !vehicle.sold }),
        elevation = 4.dp,

        ) {
        Row(
            modifier =
            Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(modifier = Modifier.width(8.dp))
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .background(BackgroundColor),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Box(modifier = Modifier.align(Alignment.CenterVertically)) {
                    Text(
                        text = vehicle.model,
                        textDecoration = if (vehicle.sold) {
                            TextDecoration.LineThrough
                        } else {
                            null
                        },
                        fontStyle = if (vehicle.sold) {
                            FontStyle.Italic
                        } else {
                            null
                        },
                        fontSize = 20.sp,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 1,
                        modifier =
                        Modifier
                            .padding(8.dp)
                    )
                }
                Box(modifier = Modifier.align(Alignment.CenterVertically)) {
                    Text(
                        text = if (vehicle.sold) {
                            "Sold"
                        } else {
                            ""
                        },
                        color = GreyLabelTextColor,
                        fontSize = 20.sp,
                        modifier =
                        Modifier
                            .padding(8.dp)

                    )
                }
            }
        }
    }
}

@Composable
fun ExpandedVehicleItemView(vehicle: VehicleModel, isExpanded: Boolean) {
    val expandTransition = remember {
        expandVertically(
            expandFrom = Alignment.Top,
            animationSpec = tween(300)
        ) + fadeIn(
            animationSpec = tween(300)
        )
    }
    val collapseTransition = remember {
        shrinkVertically(
            shrinkTowards = Alignment.Top,
            animationSpec = tween(300)
        ) + fadeOut(
            animationSpec = tween(300)
        )
    }
    AnimatedVisibility(
        visible = isExpanded,
        enter = expandTransition,
        exit = collapseTransition
    ) {
        Card(
            backgroundColor = ExpandedListBackGroundColor,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 2.dp),
            elevation = 2.dp
        ) {
            Column(
                Modifier.padding(8.dp)
            ) {
                Row() {
                    Text(text = "Type: ", color = GreyLabelTextColor)
                    Text(stringResource(vehicle.type.type))
                }
                Row() {
                    Text(text = "Price: ", color = GreyLabelTextColor)
                    Text("${vehicle.price}")
                }
                Row() {
                    Text(text = "Status: ", color = GreyLabelTextColor)
                    Text(
                        "The vehicle is ${
                            if (vehicle.sold) {
                                "sold"
                            } else {
                                "available"
                            }
                        }."
                    )
                }
            }
        }
    }
}