package com.example.pova_final_pdm.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val PrimaryColor = Color(0xFF6200ee)
val SecondaryColor = Color(0xFF8438f1)
val BackgroundColor = Color(0xFFFFFFFF)
val ExpandedListBackGroundColor = Color(0xFFf8f8f8)
val GreyLabelTextColor = Color(0xFF888888)
val AvailableColor = Color(0xFF6dff6d)
val SoldColor = Color(0xFFff3e3e)
val Black = Color(0xFF000000)